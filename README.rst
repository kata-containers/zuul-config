The Kata Containers project no longer uses Zuul for CI.
This project is therefore no longer maintained.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
previous commit with "git checkout HEAD^1".

For any further questions, please email
kata-dev@lists.katacontainers.io or join #kata-dev on Freenode.
